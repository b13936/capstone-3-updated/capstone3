import { Row, Col, Card, Button } from 'react-bootstrap'
import { Fragment, useContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

let token = localStorage.getItem("token")

console.log(window.location.search)

export default function ProductCard({productProp}){

	console.log(productProp)	

	const {productName, description, price, _id, isActive} = productProp

	const history = useNavigate()

	const { user } = useContext(UserContext)

	// console.log(productProp)


	function archive(){

			fetch('https://zuitt-permale-capstone2.herokuapp.com/api/products/archive',{
				method: 'PUT',
				headers: {
					Authorization: `Bearer ${token}`
				},
				body:JSON.stringify({
					productName: productName,
				})
			})
			.then(res => res.json())
			.then(archive => {
				console.log(archive)
				
				if(archive === true){
	
					Swal.fire({
						title: "Item Disabled",
						icon: "success",
						text: "Item was archived"
					})

					history("/products")	

				} else {

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})

				};
			})
	};

	function unarchive(){

		// const [product, setProduct] = useState({
		// 	isActive: null
		// })

			fetch('https://zuitt-permale-capstone2.herokuapp.com/api/products/unarchive',{
				method: 'PUT',
				headers: {
					Authorization: `Bearer ${token}`
				},
				body:JSON.stringify({
					productName: productName,
				})
			})
			.then(res => res.json())
			.then(unarchive => {
				console.log(unarchive)
				
				if(unarchive === true){
	
					Swal.fire({
						title: "Item Enabled",
						icon: "success",
						text: "Item was unarchived"
					})

					history("/products")	

				} else {

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})

				};
			})
	};




	return(
		<Row className="productRow d-flex flex-wrap justify-content-center pe-3 ps-3">
			<Col xs={8} md={3}>
				<Card className="m-1">
					<Card.Body>
						<Card.Title>{productName}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>

						{ (user.isAdmin === false ) ?
						<Link className="btn btn-primary" to={`/products/${_id}`}>
								Details
						</Link>

						:

						<Fragment>
						<div className = "d-flex justify-content-center">
							<Button className="primary me-2">
								Edit
							</Button>

							{ (isActive === true) ?

								<Button variant="danger" onClick={e => archive(e)}>
									Archive
								</Button>

								: 

								<Button variant="success" onClick={e => unarchive(e)}>
									Unarchive
								</Button>
							}
						</div>
						</Fragment>

						}
					</Card.Body>
				</Card>	
			</Col>
		</Row>
	)
}