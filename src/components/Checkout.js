import { Fragment, useEffect, useState, setState, useContext } from 'react'
import { Button, Row, Col } from 'react-bootstrap'
import productsData from '../data/productsData'
import CheckoutCard from './CheckoutCard'
import UserContext from '../UserContext'



let token = localStorage.getItem("token")

export default function Checkout(){

	const { qty, setQty } = useContext(UserContext) 

	const [products, setProducts] = useState([]);


	const [order, setOrders] = useState([]);
	const [order2, setOrder2] = useState([]);


	useEffect(()=>{
		fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/cart',{
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(orderList => {

			console.log(orderList)

				orderList.map(order => {
					fetch(`https://zuitt-permale-capstone2.herokuapp.com/api/products/${order.productId}`)
					.then(res => res.json())
					.then(data => {
						console.log(data) //need to conjure outside fetch to set in OrderCard
						
					});
				})

			setOrders(orderList.map(orders => {

				console.log(orders)

				return(
					<CheckoutCard key={orders._id} orderProp = {orders}/>
				)

			}))
		});

	},[])
	console.log(order2)
	console.log(order)


	const checkout = () => {
		
	}

	return (
		<Fragment>
			<div className="orderHeader">
				<h1>Checkout</h1>
			</div>
			{order}
			<Row className="m-2">
				<h5 className="d-flex justify-content-center">
					Order has been placed!
				</h5>
			</Row>

		</Fragment>
	)
}