import { Row, Col, Card, Button, InputGroup, FormControl } from 'react-bootstrap'
import { useState, useEffect, Fragment, useContext } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function OrderCard({orderProp}){

	// const { productId } = useParams()
	console.log(orderProp)

	const {productName, description, price, productId, isActive} = orderProp

	const history = useNavigate()

	const { user, setUser } = useContext(UserContext)

	const [qty, setQty] = useState(1)




function addQty(){
	setQty(qty + 1)
}

function minusQty(){
	setQty(qty - 1)
}

const range = () => {
	if (qty <= 0){
		setQty(0)
	}
}

const removeOrder = (productId) => {
		console.log(productId)

		fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/remove-order', {
			method: 'POST',
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})

			})
			.then(res => res.json())
			.then(data => {
				console.log(data)


				if(data.length !== 0){
					Swal.fire({
						title: "Removed from Cart",
						icon: "success",
						text: "Cart Updated"
					})

					history("/Cart")	

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Cart not Updated"
					})

				}
		})

	}

	return(
		<Row className="productRow d-flex flex-wrap justify-content-center pe-3 ps-3">
			<Col xs={4}>
				<Card className="m-1">
					<Card.Body>
						<Card.Title>{productName}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>


					</Card.Body>
				</Card>	
			</Col>
			<Col xs={1} className="d-flex align-items-center">
			  	<InputGroup>
					<FormControl type="number" min="1" value={qty} onChange = {e => range(e) }/>
				</InputGroup>
			</Col>
			<Col xs={2} className="d-flex align-items-center">
				<Button variant="warning" onClick={(e) => minusQty(e)}>
						-
				</Button>
				<Button variant="success" onClick={(e) => addQty(e)}>
						+
				</Button>
				<Button variant="danger" className="ms-2" onClick={() => removeOrder(productId)}>
						Remove from Cart
				</Button>
			</Col>
		</Row>



	)


}