import { Button, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Banner() {
	return(
		<Row className= "banner p-2">
			<Col className= "bannerCol p-5 d-flex align-items-end justify-content-start">
				<div>
					<h1>Tempo Shop</h1>
					<p className="d-flex">
						Classic style in temporary trends
					</p>
					<div className="d-flex">
						<Link className="btn" variant="warning" to={`/products`}>
							SHOP NOW
						</Link>
					</div>
				</div>
			</Col>
		</Row>
	)
}