import { Fragment, useEffect, useState } from 'react'
import productsData from '../data/productsData'
import OrderCard from './OrderCard'

let token = localStorage.getItem("token")

export default function Cart(){

	const [order, setOrders] = useState([]);


	useEffect(() => {
		fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/cart',{
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setOrders(data.map(orders => {
				return (
					<OrderCard key = {orders._id} orderProp ={orders}/>
				)

			}));
		});

	},[]);


	return (
		<Fragment>
			<div className="orderHeader">
				<h1>Cart</h1>
			</div>
			{order}
		</Fragment>
	)
}