import { useState, useContext, Fragment } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'

export default function AppNavbar(){

	const { user } = useContext(UserContext)
	console.log(user)

	// const [user, setUser] = useState(localStorage.getItem('email'))

	return(

		<div className="background">
			<Navbar className="Navbar">
				<Navbar.Brand as={ Link } to="/" className="logo">TEMPO</Navbar.Brand>
				<Navbar.Toggle/>
				<Navbar.Collapse id="basic-navbar-nav">

					{ (user.isAdmin == false || user.isAdmin == null) ?

					<Nav className="me-auto">
						<Nav.Link as={ Link } to="/products">Products</Nav.Link>
					</Nav>

					:

					<Fragment>
					<Nav className="me-auto">
						<Nav.Link as={ Link } to="/products">Inventory</Nav.Link>
					</Nav>
					
					</Fragment>

					}

					{ (user.isAdmin == false || user.isAdmin == null) ?

					<Nav className="mr-auto">
						<Nav.Link as={ Link } to="/Cart" className="m-2">Cart</Nav.Link>
					</Nav>

					:


					<Nav className="mr-auto">
						<Nav.Link as={ Link } to="/products" className="m-2">Total Orders</Nav.Link>
					</Nav>


					}


					{ (user.id !== null) ?


					<Nav className="mr-auto">
						<Nav.Link as={ Link } to="/logout" className="m-2">Logout</Nav.Link>
					</Nav>

					:

					<Nav className="mr-auto me-5">	
						<Nav.Link as={ Link } to="/login" className="m-2">Login</Nav.Link>
						<Nav.Link as={ Link } to="/register" className="m-2">Register</Nav.Link>
					</Nav>

					}

				</Navbar.Collapse>
			</Navbar>
		</div>
	)
}

