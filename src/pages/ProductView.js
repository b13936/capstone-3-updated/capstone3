import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function ProductView(){

	const { productId } = useParams()

	const history = useNavigate()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const { user } = useContext(UserContext)

	const purchase = (productId) => {

		// console.log(productId)

		fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/order', {
			method: 'POST',
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})

			})
			.then(res => res.json())
			.then(data => {
				console.log(data)


				if(data.length !== 0){
					Swal.fire({
						title: "Purchase Successful",
						icon: "success",
						text: "Thank you your purchase!"
					})

					history("/products")	

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})

				}
		})

	}

	useEffect(() => {
		console.log(productId)

		fetch(`https://zuitt-permale-capstone2.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [productId])


	return(
		<Container>
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description: </Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price: </Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>

							
							{ user.id !== null ?
								<Button variant="primary" onClick={() => purchase(productId)}>Add To Cart</Button>

								:

								<Link className="btn btn-danger btn-block" to="/login">Log In to Purchase</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}