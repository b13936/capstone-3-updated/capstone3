import { Fragment, useState, useEffect, useContext } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import UserContext from './../UserContext'
import Swal from 'sweetalert2'


export default function Register(){

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState(false)

	function registerUser(e){
		e.preventDefault()

		fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/register',
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data !== false){

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Registered Successfully',
					icon: 'success',
					text: 'You are not Logged In'
				})
			} else {

				Swal.fire({
					title: 'Registration Failed',
					icon: 'error',
					text: 'Check details and try again'
				})
			}

		})

		// Clear input fields
		setEmail('')
		setPassword1('')
		setPassword2('')

		// alert('Thank you for registering')
	}

	const retrieveUserDetails = (token) => {

	fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/details', {
		method: "POST",
		headers: {
			Authorization: `Bearer ${token}`
		}

	})
	.then(res => res.json())
	.then(data => {

		console.log(data)

		setUser({
			id: data._id,
			isAdmin: data.isAdmin
		})
	})
}


	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [email, password1, password2])

	return(

	(user.id !== null) ?

		<Navigate to = "/products"/>

	:

	<Fragment>
	<Row className="registerContainer d-flex justify-content-center">
		<Col xs={2}>
		<h1 className="mt-5 d-flex justify-content-center">Register</h1>
			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label className="mt-3"><h5>Email Address</h5></Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label className="mt-3"><h5>Password</h5></Form.Label>
					<Form.Control
						type="password"
						placeholder= "Enter your password here"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label className="mt-3"><h5>Confirm Password</h5></Form.Label>
					<Form.Control
						type="password"
						placeholder="Re-enter your password here"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ?	
					<Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-3">
						Submit
					</Button>

					:

					<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
						Submit
					</Button>

				}


			</Form>
		</Col>
		<Col xs={8}>
		</Col>
	</Row>
	</Fragment>
	)
}