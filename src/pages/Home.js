import { Fragment } from 'react'
import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'
import ProductCard from './../components/ProductCard'
import Products from './Products'
// import Courses from '../components/Courses.js'

export default function Home(){
	return(
			<Fragment>
				<div className="homeContainer">
					<Banner/>
					<Highlights/>
				</div>
			</Fragment>
		)
}