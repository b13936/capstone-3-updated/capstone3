import { Fragment, useState, useEffect, useContext } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import UserContext from './../UserContext'
import Swal from 'sweetalert2'


export default function Login(){

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

function loginUser(e){
	e.preventDefault()

	fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/login',
	{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}
	)
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(typeof data.access !== 'undefined'){

			localStorage.setItem('token', data.access)
			retrieveUserDetails(data.access)

			Swal.fire({
				title: 'Login Successful',
				icon: 'success',
				text: 'You may now start shopping'
			})
		} else {

			Swal.fire({
				title: 'Authentication Failed',
				icon: 'error',
				text: 'Check details and try again'
			})
		}
	})



	// localStorage.setItem('email', email)

	// setUser({

	// 	email: localStorage.getItem('email')
	// })

	setEmail('')
	setPassword('') 

	// alert(`${email} has been verified! Welcome back!`)

}

const retrieveUserDetails = (token) => {

	fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/details', {
		method: "POST",
		headers: {
			Authorization: `Bearer ${token}`
		}

	})
	.then(res => res.json())
	.then(data => {

		console.log(token)
		console.log(data)

		setUser({
			id: data._id,
			isAdmin: data.isAdmin
		})
	})
}


useEffect(() => 
{	if(email !== '' && password !== ''){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password])

	return (
		(user.id !== null) ?

			<Navigate to= "/products"/>
		:

		<Fragment>
		<Row className="registerContainer d-flex justify-content-center">
			<Col xs={2}>
				<h1 className="mt-5">Login</h1>
				<Form onSubmit={(e) => loginUser(e)}>
					<Form.Group className="mt-3" controlId="loginEmail">
						<Form.Label>Email Address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter your email here"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group className="mt-3" controlId="password">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder= "Enter your password here"
							value={password}
							onChange={e => setPassword(e.target.value)}
							required
						/>
					</Form.Group>


					{ isActive ?
						<Button variant="success" type="submit" id="loginBtn" className="mt-3">
									Submit
						</Button>

						:

						<Button variant="danger" type="submit" id="loginBtn" className="mt-3" disabled>
									Submit
						</Button>
					}

				</Form>
			</Col>
		</Row>
		</Fragment>

	)
}