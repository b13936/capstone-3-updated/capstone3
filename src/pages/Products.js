import { Fragment, useEffect, useState, useContext } from 'react'
import ProductCard from './../components/ProductCard'
import UserContext from '../UserContext'

export default function Products(){

	const [products, setProducts] = useState([]);

	const { user } = useContext(UserContext)




	useEffect(() => {

		if(user.isAdmin === true){
			fetch('https://zuitt-permale-capstone2.herokuapp.com/api/products')
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setProducts(data.map(product => {
					
					console.log(product)
					return (
						<ProductCard key = {product._id} productProp ={product}/>
					)

				}));
			});
		} else {
			fetch('https://zuitt-permale-capstone2.herokuapp.com/api/products/active-products')
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setProducts(data.map(product => {
					
					console.log(product)
					return (
						<ProductCard key = {product._id} productProp ={product}/>
					)

				}));
			});
		}

	},[user.isAdmin]);


	return (
		<Fragment>
			<div className="productHeader">
				<h1>Products</h1>
			</div>
			{products}
		</Fragment>
	)
}