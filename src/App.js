// react
import { useState, useEffect } from 'react'
import { Routes, Route } from 'react-router-dom'
import { BrowserRouter as Router } from 'react-router-dom'

// Style
import './App.css';
import { Container } from 'react-bootstrap'

// Components
import AppNavbar from './components/AppNavbar'

// Pages
import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import ProductView from './pages/ProductView'
import Cart from './components/Cart'
import Checkout from './components/Checkout'

// Context
import { UserProvider } from './UserContext'



function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => { 
    localStorage.clear()
  }

  useEffect(() => {
    // console.log(user)
    // console.log(localStorage)
    fetch('https://zuitt-permale-capstone2.herokuapp.com/api/users/details',
      {
          method: "POST",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
          }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        if(typeof data._id !=="undefined"){

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      }) 
  }, [])

  return (

      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <div>
          <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route exact path="/products" element={<Products/>}/>
            <Route exact path="/Cart" element={<Cart/>}/>
            <Route exact path="/checkout" element={<Checkout/>}/>
            <Route exact path="/register" element={<Register/>}/>
            <Route exact path="/products/:productId" element={<ProductView/>}/>

            <Route exact path="/login" element={<Login/>}/>
            <Route exact path="/logout" element={<Logout/>}/>
          </Routes>
        </div>
      </Router>
      </UserProvider>
  );
}

export default App;
